# Bitbucket Pipelines Pipe: AWS EKS run command

Run a command against an AWS EKS cluster. This pipe uses [kubectl][kubectl], a command line interface for running commands against Kubernetes clusters.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-eks-kubectl-run:3.1.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    AWS_OIDC_ROLE_ARN: "<string>" # Optional by default. Required for OpenID Connect (OIDC) authentication.
    CLUSTER_NAME: '<string>'
    KUBECTL_COMMAND: '<string>'
    # AWS_ROLE_ARN: '<string>' # Optional.
    # AWS_ROLE_SESSION_NAME: '<string>' # Optional.
    # KUBECTL_ARGS: '<array>' # Optional
    # KUBECTL_APPLY_ARGS: '<string>' # Optional
    # RESOURCE_PATH: 'string' # Optional
    # LABELS: '<array>' # Optional
    # WITH_DEFAULT_LABELS: '<boolean>' # Optional
    # DISABLE_VALIDATION: '<boolean>' # Optional
    # PRE_EXECUTION_SCRIPT: '<string>' # Optional
    # DEBUG: '<boolean>' # Optional
```
## Variables

| Variable                   | Usage                                                                                                                                                                                                                                                                                  |
|----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID (**)     | AWS access key id.                                                                                                                                                                                                                                                                     |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret access key.                                                                                                                                                                                                                                                                 |
| AWS_DEFAULT_REGION (**)    | AWS region.                                                                                                                                                                                                                                                                            |
| AWS_OIDC_ROLE_ARN          | The ARN of the role you want to assume. Required for OpenID Connect (OIDC) authentication. See **AWS Authentication**.                                                                                                                                                                 |
| CLUSTER_NAME (*)           | The name of a kubernetes cluster.                                                                                                                                                                                                                                                      |
| KUBECTL_COMMAND (*)        | Kubectl command to run. For more details you can check the [kubectl reference guide][kubectl reference guide]                                                                                                                                                                          |
| AWS_ROLE_ARN               | Specifies the Amazon Resource Name (ARN) of an IAM role with a web identity provider that you want to use to run the AWS commands. See **EKS Authentication**.                                                                                                                         |
| AWS_ROLE_SESSION_NAME      | Specifies the name to attach to the role session. This value is provided to the RoleSessionName parameter when the AWS CLI calls the AssumeRole operation, and becomes part of the assumed role user ARN: `arn:aws:sts::123456789012:assumed-role/role_name/role_session_name`.        |
| KUBECTL_ARGS               | Arguments to pass to the kubectl command. Default: `null`                                                                                                                                                                                                                              |
| KUBECTL_APPLY_ARGS         | Arguments to pass after the kubectl `apply` command. Default: `-f`. For more details check out the [kubectl apply guide][kubectl apply guide]                                                                                                                                          |
| RESOURCE_PATH              | Path to the kubernetes spec file. This option is required only if the KUBECTL_COMMAND is `apply`                                                                                                                                                                                       |
| LABELS                     | Key=value pairs that are attached to a Deployment. Labels are intended to be used to specify identifying attributes of objects. Valid labels must follow [Syntax and character set][Syntax and character set]. Format: `key=value`. Example: `bitbucket.org/bitbucket_branch=develop`. |
| WITH_DEFAULT_LABELS        | Whether or not to add the default labels. Check Labels added by default section for more details.                                                                                                                                                                                      |
| DISABLE_VALIDATION         | Set this variable to `true` to disable validation of your Kubernetes manifest file before the command execution. Default `false`.                                                                                                                                                      |
| PRE_EXECUTION_SCRIPT       | Path to pre-execution script to execute additional specific actions needed.                                                                                                                                                                                                            |
| DEBUG                      | Turn on extra debug information. Default: `false`.                                                                                                                                                                                                                                     |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## EKS Authentication

When using EKS, you have to remember, that IAM principals (roles and users) don't make any sense inside a EKS cluster. You can allow IAM principals to access Kubernetes objects on your cluster using one of the following methods:

1. Creating [Access entries][Access entries] (option recommended by AWS if your cluster is at or later than the platform version listed in the [Prerequisites section][Prerequisites section]). 
  > Use access entries to manage the Kubernetes permissions of IAM principals from outside the cluster. You can add and manage access to the cluster by using the EKS API, AWS Command Line Interface, AWS SDKs, AWS CloudFormation, and AWS Management Console. This means you can manage users with the same tools that you created the cluster with.
2. Adding entries to the aws-auth ConfigMap as described in [Managing Users or IAM Roles for your Cluster][Managing Users or IAM Roles for your Cluster].
It is important to know, that only the IAM user who created the cluster has permissions to access it. Refer to the page linked above for how to grant your IAM users and roles permissions within the EKS cluster. From AWS docs:
  > When you create an Amazon EKS cluster, the IAM entity user or role, such as a federated user that creates the cluster, is automatically granted system:masters permissions in the cluster's RBAC configuration. To grant additional AWS users or roles the ability to interact with your cluster, you must edit the aws-auth ConfigMap within Kubernetes.


## AWS Authentication

Supported the next options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Used by default.
2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you setup OIDC before:
    - configure Bitbucket Pipelines as a Web Identity Provider in AWS
    - attach to provider your AWS role with required policies in AWS
    - create an access entries for your AWS EKS cluster
    - setup a build step with `oidc: true` in your Bitbucket Pipelines
    - provide AWS_OIDC_ROLE_ARN variable to the pipe


## Labels added by default

By default, the pipe will use the following labels in order to track which pipeline created the Kubernetes resources and be able to link it back to

| Label                                            | Description                                                                                                |
|--------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| `bitbucket.org/bitbucket_commit`                 | The commit hash of a commit that kicked off the build. Example: `7f777ed95a19224294949e1b4ce56bbffcb1fe9f` |
| `bitbucket.org/bitbucket_deployment_environment` | The name of the environment which the step deploys to. This is only available on deployment steps.         | 
| `bitbucket.org/bitbucket_repo_owner`             | The name of the owner account.                                                                             |
| `bitbucket.org/bitbucket_repo_slug`              | Repository name.                                                                                           |
| `bitbucket.org/bitbucket_build_number`           | Bitbucket Pipeline number                                                                                  |
| `bitbucket.org/bitbucket_step_triggerer_uuid`    | UUID from the user who triggered the step execution.                                                       |
| `bitbucket.org/bitbucket_branch`                 | Repository branch name. Will be automatically truncated when length exceeds 63 characters.                 |

## Prerequisites
 - Basic knowledge is required of how Kubernetes works and how to create services and deployments on it.
 - Kubernetes cluster running in EKS is required to use this pipe. Check out this [getting started][getting started] guide from AWS. 
 - A docker registry (Docker Hub or similar) to store your docker image: if you are deploying to a Kubernetes cluster you will need a docker registry to store you images.

## Examples 

### Basic example:

Run command 'apply' against an AWS EKS cluster.

```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:3.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
```

Basic example. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:3.1.1
    variables:
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
```

Run command 'apply' with the assumed role user ARN.

```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:3.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      AWS_ROLE_ARN: 'arn:aws:iam::012349999999:role/my-assumed-role'
      AWS_ROLE_SESSION_NAME: 'my-assumed-role'
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
```

Example with disabling validation of your spec file:
```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:3.1.1
    variables:
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      DISABLE_VALIDATION: 'true'
```

Example passing preexecution hook script file (file should have at least read and execute permissions):

```yaml
script:
  - echo 'script logic' > .my-script.sh
  - chmod 005 .my-script.sh
  - pipe: atlassian/aws-eks-kubectl-run:3.1.1
    variables:
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      PRE_EXECUTION_SCRIPT: '.my-script.sh'
```

### Advanced example:

Run command 'apply' against a AWS EKS cluster with OpenID Connect (OIDC) alternative authentication.
Parameter "oidc: true" in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required for OIDC authentication:

```yaml
- step:
    oidc: true
    script:
      - pipe: atlassian/aws-eks-kubectl-run:3.1.1
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          CLUSTER_NAME: 'my-kube-cluster'
          KUBECTL_COMMAND: 'apply'
          RESOURCE_PATH: 'nginx.yml'
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
```

Passing KUBECTL_ARGS:

```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:3.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      KUBECTL_ARGS:
        - "--dry-run"
```

Providing custom labels:

```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:3.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'nginx.yml'
      LABELS:
        - 'environment=production'
        - 'tier=backend'
```

Passing KUBECTL_APPLY_ARGS:

```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:3.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      RESOURCE_PATH: 'k8s/dev/kustomization'
      KUBECTL_APPLY_ARGS: '-k'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,aws,eks,kubernetes
[kubectl]: https://kubernetes.io/docs/reference/kubectl/overview/
[kubectl reference guide]: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands
[kubectl apply guide]: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply
[Syntax and character set]: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set
[Managing Users or IAM Roles for your Cluster]: https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html
[getting started]: https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html
[Access entries]: https://docs.aws.amazon.com/eks/latest/userguide/access-entries.html
[Prerequisites section]: https://docs.aws.amazon.com/eks/latest/userguide/access-entries.html#access-entries-prerequisites
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect/
