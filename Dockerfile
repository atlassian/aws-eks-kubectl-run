FROM python:3.10-slim-buster as build

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

COPY requirements.txt /
RUN apt-get update && apt-get install --no-install-recommends -y  \
      apt-transport-https=1.8.*  \
      gnupg=2.*  \
      curl=7.*  \
      unzip=6.*  \
      git=1:2.*
RUN mkdir -p /etc/apt/keyrings
RUN echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.31/deb/ /" | tee /etc/apt/sources.list.d/kubernetes.list  && \
    curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.31/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg && \
    apt-get update && apt-get install --no-install-recommends -y kubectl=1.31.*  && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.22.27.zip" -o "awscliv2.zip"  && \
    echo 'b620a51f6a97e7443dc44527b179597fcc2cb3dc2103b7ac79ca6acc73123e44  awscliv2.zip' | sha256sum -c -  && \
    unzip awscliv2.zip
RUN pip install --user --no-cache-dir -r /requirements.txt


FROM python:3.10-slim-buster

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

WORKDIR /

# add additional packages
RUN apt-get update && apt-get install --no-install-recommends -y \
      git=1:2.* && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# copy kubectl cli binary and install aws cli v2
COPY --from=build /usr/bin/kubectl /usr/bin
COPY --from=build /aws aws
RUN ./aws/install && rm -rf aws

# copy python env
COPY --from=build /root/.local /root/.local
# copy project files
COPY pipe/ requirements.txt pipe.yml LICENSE.txt README.md /

ENV PATH=/root/.local/bin:$PATH

ENTRYPOINT ["python", "/pipe.py"]
