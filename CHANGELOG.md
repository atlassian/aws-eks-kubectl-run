# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 3.1.1

- patch: Update the Readme. Fix the link in the Readme.

## 3.1.0

- minor: Bump awscli version to 2.27.27.
- minor: Bump kubectl  version to 1.31.
- minor: Internal maintenance: bump atlassian/kubectl-run to 3.12.0.
- patch: Internal maintenance: Update pipes versions in pipelines config file.

## 3.0.0

- major: Implemented support for AWS STS (using assume-role functionality). Breaking change! Variable ROLE_ARN renamed to AWS_ROLE_ARN.
- patch: Internal maintenance: Update pipelines config file.

## 2.9.0

- minor: Internal maintenance: bump atlassian/kubectl-run to 3.11.0.
- patch: Internal maintenance: update pipes versions in pipelines config file.

## 2.8.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.8.0

- minor: Bump awscli version to 2.15.15.
- minor: Bump kubectl version to 1.29.
- patch: Internal maintenance: update pipes versions in pipelines config file.

## 2.7.0

- minor: Add support for AWS OIDC authorisation.

## 2.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.5.1

- patch: Update documentation: fix examples.

## 2.5.0

- minor: Add support for DISABLE_VALIDATION variable. Option to disable Kubernetes manifest file validation before the command execution..
- minor: Add support of the pre-execution script.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 2.4.0

- minor: Bump kubectl cli to version 1.26
- patch: Bump aws cli to version 2.9.5

## 2.3.0

- minor: Internal maintenance: bump atlassian/kubectl-run to 3.6.0.
- minor: Internal maintenance: bump docker image in Dockerfile to python:3.10-slim-buster.
- patch: Bugfix: fix LABELS not passed correctly.
- patch: Internal maintenance: bump docker image and pipes versions in pipelines config file.

## 2.2.1

- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Update the community link.

## 2.2.0

- minor: Add support for ROLE_ARN parameter.

## 2.1.0

- minor: Internal maintenance: make own Dockerfile independent of kubectl-run pipe.
- minor: Upgrade kubectl lib 1.19 -> 1.21. See kubernetes release notes in https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html.

## 2.0.0

- major: Update base dependency kubectl-run to version 3.0.1: Bumped AWS CLI v1 -> v2, bitbucket-pipes-toolkit -> 2.2.0

## 1.4.2

- patch: Internal maintenance: Bump test requirement bitbucket-pipes-toolkit -> 2.2.0.

## 1.4.1

- patch: Fix bug with LABELS variable. Labels are intended to be used to specify identifying attributes of Kubernetes objects.

## 1.4.0

- minor: Bump kubectl-run pipe 1.1.1-> 1.3.0.

## 1.3.1

- patch: Internal maintenance: bump pipe-release.

## 1.3.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.2.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.2.3

- patch: Internal maintenance: Add gitignore secrets.

## 1.2.2

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.2.1

- patch: Added more details about Kubernetes access controls to the README
- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 1.2.0

- minor: Add default values for AWS variables.

## 1.1.1

- patch: Internal maintenance: Add check for newer version.

## 1.1.0

- minor: The pipe now accepts directory path in RESOURCE_PATH variable

## 1.0.0

- major: Changed the SPEC_FILE varialbe to RESOURCE_PATH

## 0.2.1

- patch: Documentation improvements.

## 0.2.0

- minor: Fixed the docker image reference

## 0.1.0

- minor: Initial release
- patch: Updated the base pipe version
