import configparser
import logging
import os
import shutil
import sys
from copy import copy
from unittest import mock, TestCase

import pytest

from pipe.pipe import EKSDeployPipe, schema


class EKSTestCase(TestCase):
    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    @mock.patch.dict(os.environ, {
        'AWS_ACCESS_KEY_ID': 'test_key',
        'AWS_SECRET_ACCESS_KEY': 'test_secret_key',
        'AWS_DEFAULT_REGION': 'default',
        'KUBECTL_COMMAND': 'apply',
        'KUBECTL_APPLY_ARGS': '-test-args',
        'CLUSTER_NAME': 'test-cluster',
        'DISABLE_VALIDATION': 'true',
        'RESOURCE_PATH': 'test-path',
        'ROLE_ARN': 'test-role'
    })
    @mock.patch('subprocess.run')
    @mock.patch('pipe.pipe.EKSDeployPipe.validate_spec_file')
    @mock.patch('pipe.pipe.EKSDeployPipe.compile_labels')
    def test_eks_run_without_validation(self, labels_mock, validate_mock, subprocess_mock):
        subprocess_mock.return_value = mock.Mock(returncode=0)
        labels_mock.return_value = None
        validate_mock.return_value = None

        EKSDeployPipe(schema=schema, check_for_newer_version=True).handle_apply()

        validate_mock.assert_not_called()

        subprocess_mock.assert_called_once_with(
            ['kubectl', 'apply', '-test-args', 'test-path', '--validate=false'], stdout=sys.stdout)


class EKSDeployAuthTestCase(TestCase):

    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog):
        self.caplog = caplog

    @classmethod
    def setUpClass(cls):
        cls.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    @classmethod
    def tearDownClass(cls):
        sys.path = cls.sys_path
        if os.path.exists(os.path.join(os.environ["HOME"], '.aws')):
            shutil.rmtree(os.path.join(os.environ["HOME"], '.aws'))

    @mock.patch.dict(os.environ, {
        'AWS_ACCESS_KEY_ID': 'akiafkae',
        'AWS_SECRET_ACCESS_KEY': 'secretkey',
        'AWS_DEFAULT_REGION': 'test-region',
        'CLUSTER_NAME': 'test-cluster',
        'KUBECTL_COMMAND': 'test_command',
    })
    def test_discover_auth_method_default_credentials_only(self):

        ecs_pipe = EKSDeployPipe(schema=schema, check_for_newer_version=True)

        with self.caplog.at_level(logging.INFO):
            ecs_pipe.auth()

        self.assertIn("Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY", self.caplog.text)
        self.assertEqual(ecs_pipe.auth_method, 'DEFAULT_AUTH')
        self.assertFalse(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))

    @mock.patch.dict(os.environ, {
        'BITBUCKET_STEP_OIDC_TOKEN': 'token',
        'AWS_OIDC_ROLE_ARN': 'account/role',
        'AWS_DEFAULT_REGION': 'test-region',
        'CLUSTER_NAME': 'test-cluster',
        'KUBECTL_COMMAND': 'test_command',
    })
    def test_discover_auth_method_oidc_only(self):

        ecs_pipe = EKSDeployPipe(schema=schema, check_for_newer_version=True)

        with self.caplog.at_level(logging.INFO):
            ecs_pipe.auth()

        self.assertIn("Authenticating with a OpenID Connect (OIDC) Web Identity Provider", self.caplog.text)
        self.assertEqual(ecs_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))

        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    @mock.patch.dict(os.environ, {
        'BITBUCKET_STEP_OIDC_TOKEN': 'token',
        'AWS_OIDC_ROLE_ARN': 'account/role',
        'AWS_ACCESS_KEY_ID': 'akiafkae',
        'AWS_SECRET_ACCESS_KEY': 'secretkey',
        'AWS_DEFAULT_REGION': 'test-region',
        'CLUSTER_NAME': 'test-cluster',
        'KUBECTL_COMMAND': 'test_command',
    })
    def test_discover_auth_method_oidc_and_default_credentials(self):
        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)

        ecs_pipe = EKSDeployPipe(schema=schema, check_for_newer_version=True)

        with self.caplog.at_level(logging.INFO):
            ecs_pipe.auth()

        self.assertIn("Authenticating with a OpenID Connect (OIDC) Web Identity Provider", self.caplog.text)
        self.assertEqual(ecs_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)
