import os
import sys
import time
import stat
import subprocess
import configparser

import boto3

from kubectl_run.pipe import KubernetesDeployPipe, logger


schema = {
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},
    'CLUSTER_NAME': {'type': 'string', 'required': True},
    'KUBECTL_COMMAND': {'type': 'string', 'required': True},
    'AWS_ROLE_ARN': {'type': 'string', 'required': False},
    'AWS_ROLE_SESSION_NAME': {'type': 'string', 'required': False},
    'KUBECTL_ARGS': {'type': 'list', 'required': False, 'default': []},
    'KUBECTL_APPLY_ARGS': {'type': 'string', 'required': False, 'default': '-f'},
    'RESOURCE_PATH': {'type': 'string', 'required': False, 'nullable': True, 'default': ''},
    'LABELS': {'type': 'list', 'required': False},
    'WITH_DEFAULT_LABELS': {'type': 'boolean', 'required': False, 'default': True},
    'DISABLE_VALIDATION': {'type': 'boolean', 'required': False, 'default': False},
    'PRE_EXECUTION_SCRIPT': {'type': 'string', 'required': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class EKSDeployPipe(KubernetesDeployPipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, *args, **kwargs):
        self.auth_method = self.discover_auth_method()
        self.role_arn_check = self.resolve_role_arn()
        super().__init__(*args, **kwargs)

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                logger.info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider')
                schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                schema['AWS_ACCESS_KEY_ID']['required'] = False
                schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                # unset env variables to prevent aws client general auth
                # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html
                if 'AWS_ACCESS_KEY_ID' in os.environ:
                    del os.environ['AWS_ACCESS_KEY_ID']
                if 'AWS_SECRET_ACCESS_KEY' in os.environ:
                    del os.environ['AWS_SECRET_ACCESS_KEY']
                if 'AWS_SESSION_TOKEN' in os.environ:
                    del os.environ['AWS_SESSION_TOKEN']

                return self.OIDC_AUTH

            logger.warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')
            logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
            return self.DEFAULT_AUTH

        logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
        return self.DEFAULT_AUTH

    @staticmethod
    def resolve_role_arn():
        if os.getenv('AWS_ROLE_ARN'):
            schema['AWS_ROLE_ARN']['required'] = True
            schema['AWS_ROLE_SESSION_NAME']['required'] = True

            return True

        return False

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(oidc_token_directory, f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))

            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)
            logger.debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)
            logger.debug('Configured settings for authentication with assume web identity role')

    def configure(self):
        self.log_info("Configuring kubeconfig...")

        cluster_name = self.get_variable("CLUSTER_NAME")

        cmd = f'aws eks update-kubeconfig --name={cluster_name}'.split()

        role = self.get_variable('AWS_ROLE_ARN')
        if role is not None:
            logger.info('Using STS assume role with AWS_ROLE_ARN')

            client = boto3.client('sts')
            response = client.assume_role(
                RoleArn=role,
                RoleSessionName=self.get_variable('AWS_ROLE_SESSION_NAME')
            )

            os.environ["AWS_ACCESS_KEY_ID"] = response['Credentials']['AccessKeyId']
            os.environ["AWS_SECRET_ACCESS_KEY"] = response['Credentials']['SecretAccessKey']
            os.environ["AWS_SESSION_TOKEN"] = response['Credentials']['SessionToken']

        if self.get_variable('DEBUG'):
            cmd.append("--verbose")

        result = subprocess.run(cmd, stdout=sys.stdout)

        if result.returncode != 0:
            self.fail('Failed to update the kube config.')
        else:
            self.log_info('Successfully updated the kube config.')


if __name__ == '__main__':
    pipe = EKSDeployPipe(schema=schema, pipe_metadata_file='/pipe.yml', check_for_newer_version=True)
    pipe.auth()
    pipe.run()
